# Pipeline gitlab
## Prérequis

### Gitlab.com

Pour ces TP, nous allons utiliser https://gitlab.com et pas l'instance universitaire https://gitlab.univ-lille.fr en effet certaines fonctionnalités n'ont pas été activées sur l'instance universitaire.

Vous avez toutes et tous reçu une invitation à vous créer un compte ou ajouter votre mail universitaire sur votre compte gitlab.com, ce qui vous donne accès un groupe de la forme https://gitlab.com/univlille/iut-info/r5.04/2023/nom-prenom

Dans ce groupe, faire un fork (une divergence) de ce projet que vous nommerez `tp1` dans lequel vous allez travailler.

### Docker sans Docker natif

Ce projet contient un `Vagrantfile` et le script [bootstrap.sh](./bootstrap.sh) qui permet :
* D'initialiser en une commande une VM permettant de faire tourner docker.
* De partager le dossier courant dans la VM dans le dossier `~/workspace/`. Ce qui vous permet de modifier vos fichiers depuis la machine hôte et de les exploiter dans la VM.
* N'oubliez pas de sourcer le fichier `/home/public/vm/vagrant.env`

## Q0 - VM vagrant

* vérifier que vous arrivez bien à vous connecter sur votre VM vagrant
* qu'elle permet de lancer des containers docker
* et que quand vous modifiez un fichier dans le dossier courant il est bien visible dans la VM.

## Q1 - gitlab pages static

Créer un premier job pour voir apparaitre la page html suivante sur https://univlille.gitlab.io/iut-info/r5.04/2023/groupe-X/nom-prenom/tp1/
```html
 <html>
 <head>
   <title>TP1 de Prénom Nom</title>
 </head>
 <body>
   <h1>Pipeline gitlab</h1>
   <ul>
    <li><a href="readme.html">Le sujet</a></li>
   </ul>
 </body>
 </html>
```

## Q1.1 - Une seule branche

Faire évoluer la chaine d'intégration continue pour ne déployer que la branche principale.

## Q1.2 - Un site public

Rendre le site web accessible pour tous.

## Q2 - gitlab pages avec pandoc

Modifier votre job pour qu'il génère le fichier readme.html à publier sur https://univlille.gitlab.io/iut-info/r5.04/2023/nom-prenom/tp1/ depuis le fichier README.md

Pour information il est possible de générer du html depuis un fichier Markdown en utilisant [pandoc](https://pandoc.org/). La commande à utiliser est alors : 
``̀`shell
pandoc --standalone -f markdown+smart < fichier.md > fichier.html
``̀`

Avec l'option `--css=style.css` il est possible de spécifier un fichier de style.

## Q3 - Dockerfile pour l'app

Construire un `Dockerfile` pour packager l'application.

Pour information, c'est un projet en java et les dépendances sont gérées par maven, vous pouvez donc utiliser ces commandes : 

* Commande pour construire le projet : `mvn install -Dmaven.test.skip=true`
* Commande pour lancer le service après compilation : `java -jar GetThingsDone-app/target/GetThingsDone-app-1.0-SNAPSHOT.jar`
* Le service écoute sur le port 8080 de toutes les interfaces en http

## Q4 - registry.gitlab.com

* Ajouter un job indépendant du précédent pour construire le container de l'application et l'enregistrer sur la registry de gitlab.com.
* ce job doit conditionner la publication du site web
* Vérifier que vous arrivez bien à télécharger et exécuter votre container depuis votre VM

## Q5 - versions

* Créer un job pour fabriquer une version à chaque pose de tag.
* La note de version devra contenir tous les messages de commit depuis la dernière version

## Q5 - docker-compose

Une fois compilée, l'application se lance avec la commande suivante : `SPRING_DATASOURCE_URL="jdbc:postgresql://dbhost:5432/getthingsdone" SPRING_DATASOURCE_USERNAME=getthingsdone SPRING_DATASOURCE_PASSWORD=mypassword java -jar GetThingsDone-app/target/GetThingsDone-app-1.0-SNAPSHOT.jar`

* ***SPRING_DATASOURCE_URL*** décrit la façon de se connecter à la base de données. Dans l'exemple une base postgres sur la machine `dbhost`
* ***SPRING_DATASOURCE_USERNAME*** décrit le username à utiliser pour se connecter à la base
* ***SPRING_DATASOURCE_PASSWORD*** décrit le mot de passe à utiliser pour se connecter à la base

construire un fichier `docker-compose.yml` permetant de lancer notre application avec une base de donnée postgres. Attention de bien utiliser d'une part le container précédement construit et enregistré sur la registry gitlab.com et d'autre part le container officiel postgres

